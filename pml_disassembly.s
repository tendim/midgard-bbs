Starting address=a010


A010  4C 76 A8   JMP  $A876                 BASIC ROM

A013  4C 77 A2   JMP  $A277                 BASIC ROM

A016  4C A6 A2   JMP  $A2A6                 BASIC ROM

A019  4C 49 CC   JMP  $CC49                 

A01C  4C B8 A2   JMP  $A2B8                 BASIC ROM

A01F  4C D8 A2   JMP  $A2D8                 BASIC ROM

A022  4C A4 A8   JMP  $A8A4                 BASIC ROM

A025  4C 0D A3   JMP  $A30D                 BASIC ROM

A028  4C E6 A3   JMP  $A3E6                 BASIC ROM

A02B  4C 26 A4   JMP  $A426                 BASIC ROM

A02E  4C BC A4   JMP  $A4BC                 BASIC ROM

A031  4C DB A7   JMP  $A7DB                 BASIC ROM

A034  4C 3B A8   JMP  $A83B                 BASIC ROM

A037  4C 44 A8   JMP  $A844                 BASIC ROM

A03A  4C 2F A6   JMP  $A62F                 BASIC ROM

A03D  4C 93 A6   JMP  $A693                 BASIC ROM

A040  4C 06 A9   JMP  $A906                 Routine: Search ':' char in string (position)

A043  4C 0A AB   JMP  $AB0A                 BASIC ROM

A046  4C 17 AC   JMP  $AC17                 BASIC ROM

A049  C9 11      CMP  #$11                  
A04B  D0 02      BNE  $A04F                 BASIC ROM
A04D  A9 0D      LDA  #$0D                  
WA04F:
A04F  C9 01      CMP  #$01                  
A051  D0 02      BNE  $A055                 BASIC ROM
A053  A9 22      LDA  #$22                  
WA055:
A055  60         RTS                        

A056  C9 14      CMP  #$14                  
A058  D0 02      BNE  $A05C                 BASIC ROM
A05A  A9 08      LDA  #$08                  
WA05C:
A05C  C9 41      CMP  #$41                  
A05E  90 06      BCC  $A066                 BASIC ROM
A060  C9 5B      CMP  #$5B                  
A062  B0 03      BCS  $A067                 BASIC ROM
A064  09 20      ORA  #$20                  
WA066:
A066  60         RTS                        

WA067:
A067  C9 60      CMP  #$60                  
A069  90 FB      BCC  WA066                 BASIC ROM
A06B  C9 C0      CMP  #$C0                  
A06D  F0 10      BEQ  $A07F                 BASIC ROM
A06F  B0 03      BCS  $A074                 BASIC ROM
A071  A9 20      LDA  #$20                  
A073  60         RTS                        

WA074:
A074  C9 DF      CMP  #$DF                  
A076  B0 F9      BCS  $A071                 BASIC ROM
A078  C9 DB      CMP  #$DB                  
A07A  B0 03      BCS  WA07F                 BASIC ROM
A07C  29 7F      AND  #$7F                  
A07E  60         RTS                        

WA07F:
A07F  49 A0      EOR  #$A0                  
A081  60         RTS                        

A082  20 E4 FF   JSR  $FFE4                 Routine: Take the char from keyboard buffer
A085  09 00      ORA  #$00                  
A087  F0 07      BEQ  $A090                 BASIC ROM
A089  C9 A0      CMP  #$A0                  
A08B  D0 1D      BNE  $A0AA                 BASIC ROM
A08D  A9 20      LDA  #$20                  
A08F  60         RTS                        

WA090:
A090  AE B7 02   LDX  $02B7                 Not used
A093  D0 15      BNE  WA0AA                 BASIC ROM
A095  A2 05      LDX  #$05                  
A097  20 C6 FF   JSR  $FFC6                 Routine: Open an input canal
A09A  20 E4 FF   JSR  $FFE4                 Routine: Take the char from keyboard buffer
A09D  48         PHA                        
A09E  20 CC FF   JSR  $FFCC                 Routine: Close the input and output channel
A0A1  68         PLA                        
A0A2  29 7F      AND  #$7F                  
A0A4  C9 7F      CMP  #$7F                  
A0A6  D0 05      BNE  $A0AD                 BASIC ROM
A0A8  A9 14      LDA  #$14                  
WA0AA:
A0AA  09 00      ORA  #$00                  
A0AC  60         RTS                        

WA0AD:
A0AD  C9 08      CMP  #$08                  
A0AF  F0 F7      BEQ  $A0A8                 BASIC ROM
A0B1  C9 41      CMP  #$41                  
A0B3  90 F5      BCC  WA0AA                 BASIC ROM
A0B5  C9 5B      CMP  #$5B                  
A0B7  B0 03      BCS  $A0BC                 BASIC ROM
A0B9  09 80      ORA  #$80                  
A0BB  60         RTS                        

WA0BC:
A0BC  C9 60      CMP  #$60                  
A0BE  90 EA      BCC  WA0AA                 BASIC ROM
A0C0  F0 07      BEQ  $A0C9                 BASIC ROM
A0C2  C9 7B      CMP  #$7B                  
A0C4  B0 03      BCS  WA0C9                 BASIC ROM
A0C6  29 DF      AND  #$DF                  
A0C8  60         RTS                        

WA0C9:
A0C9  49 A0      EOR  #$A0                  
A0CB  60         RTS                        

A0CC  48         PHA                        
A0CD  20 49 A0   JSR  $A049                 BASIC ROM
A0D0  20 E3 A0   JSR  $A0E3                 BASIC ROM
A0D3  C9 14      CMP  #$14                  
A0D5  D0 0A      BNE  $A0E1                 BASIC ROM
A0D7  A9 20      LDA  #$20                  
A0D9  20 E3 A0   JSR  WA0E3                 BASIC ROM
A0DC  A9 14      LDA  #$14                  
A0DE  20 E3 A0   JSR  WA0E3                 BASIC ROM
WA0E1:
A0E1  68         PLA                        
A0E2  60         RTS                        

WA0E3:
A0E3  20 2E A7   JSR  $A72E                 BASIC ROM
A0E6  48         PHA                        
A0E7  AD B7 02   LDA  $02B7                 Not used
A0EA  D0 23      BNE  $A10F                 BASIC ROM
A0EC  8A         TXA                        
A0ED  48         PHA                        
A0EE  98         TYA                        
A0EF  48         PHA                        
A0F0  A2 05      LDX  #$05                  
A0F2  20 C9 FF   JSR  $FFC9                 Routine: Open an output canal
A0F5  BA         TSX                        
A0F6  BD 03 01   LDA  $0103,X               CPU stack/Tape error/Floating conversion area
A0F9  20 56 A0   JSR  $A056                 BASIC ROM
A0FC  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
A0FF  C9 0D      CMP  #$0D                  
A101  D0 05      BNE  $A108                 BASIC ROM
A103  A9 0A      LDA  #$0A                  
A105  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
WA108:
A108  20 CC FF   JSR  $FFCC                 Routine: Close the input and output channel
A10B  68         PLA                        
A10C  A8         TAY                        
A10D  68         PLA                        
A10E  AA         TAX                        
WA10F:
A10F  68         PLA                        
A110  60         RTS                        

A111  A9 41      LDA  #$41                  
A113  85 57      STA  $57                   Scratch for numeric operation
A115  A5 2D      LDA  $2D                   Pointer: BASIC starting variables
A117  85 FD      STA  $FD                   
A119  A5 2E      LDA  $2E                   Pointer: BASIC starting variables
A11B  85 FE      STA  $FE                   Free 0 page for user program
A11D  A0 00      LDY  #$00                  
A11F  B1 FD      LDA  ($FD),Y               
A121  C5 57      CMP  $57                   Scratch for numeric operation
A123  D0 07      BNE  $A12C                 BASIC ROM
A125  C8         INY                        
A126  B1 FD      LDA  ($FD),Y               
A128  C9 80      CMP  #$80                  
A12A  F0 0D      BEQ  $A139                 BASIC ROM
WA12C:
A12C  A9 07      LDA  #$07                  
A12E  18         CLC                        
A12F  65 FD      ADC  $FD                   
A131  85 FD      STA  $FD                   
A133  90 E8      BCC  $A11D                 BASIC ROM
A135  E6 FE      INC  $FE                   Free 0 page for user program
A137  D0 E4      BNE  WA11D                 BASIC ROM
WA139:
A139  C8         INY                        
A13A  B1 FD      LDA  ($FD),Y               
A13C  8D AF 02   STA  $02AF                 Not used
A13F  C8         INY                        
A140  B1 FD      LDA  ($FD),Y               
A142  85 FB      STA  $FB                   Free 0 page for user program
A144  C8         INY                        
A145  B1 FD      LDA  ($FD),Y               
A147  85 FC      STA  $FC                   
A149  60         RTS                        

A14A  09 80      ORA  #$80                  
A14C  85 57      STA  $57                   Scratch for numeric operation
A14E  A5 2D      LDA  $2D                   Pointer: BASIC starting variables
A150  85 FD      STA  $FD                   
A152  A5 2E      LDA  $2E                   Pointer: BASIC starting variables
A154  85 FE      STA  $FE                   Free 0 page for user program
A156  A0 00      LDY  #$00                  
A158  B1 FD      LDA  ($FD),Y               
A15A  C9 C1      CMP  #$C1                  
A15C  D0 07      BNE  $A165                 BASIC ROM
A15E  C8         INY                        
A15F  B1 FD      LDA  ($FD),Y               
A161  C5 57      CMP  $57                   Scratch for numeric operation
A163  F0 0D      BEQ  $A172                 BASIC ROM
WA165:
A165  A9 07      LDA  #$07                  
A167  18         CLC                        
A168  65 FD      ADC  $FD                   
A16A  85 FD      STA  $FD                   
A16C  90 E8      BCC  $A156                 BASIC ROM
A16E  E6 FE      INC  $FE                   Free 0 page for user program
A170  D0 E4      BNE  WA156                 BASIC ROM
WA172:
A172  A9 02      LDA  #$02                  
A174  18         CLC                        
A175  65 FD      ADC  $FD                   
A177  85 FD      STA  $FD                   
A179  90 02      BCC  $A17D                 BASIC ROM
A17B  E6 FE      INC  $FE                   Free 0 page for user program
WA17D:
A17D  60         RTS                        

A17E  A9 30      LDA  #$30                  
A180  20 4A A1   JSR  $A14A                 BASIC ROM
A183  A9 00      LDA  #$00                  
A185  A8         TAY                        
A186  91 FD      STA  ($FD),Y               
A188  C8         INY                        
A189  91 FD      STA  ($FD),Y               
A18B  A4 59      LDY  $59                   Scratch for numeric operation
A18D  AE B2 02   LDX  $02B2                 Not used
A190  F0 14      BEQ  $A1A6                 BASIC ROM
A192  A2 00      LDX  #$00                  
A194  EC AD 02   CPX  $02AD                 Not used
A197  F0 0D      BEQ  WA1A6                 BASIC ROM
A199  BD 00 AE   LDA  $AE00,X               BASIC ROM
A19C  20 CC A0   JSR  $A0CC                 BASIC ROM
A19F  99 00 AF   STA  $AF00,Y               BASIC ROM
A1A2  C8         INY                        
A1A3  E8         INX                        
A1A4  D0 EE      BNE  $A194                 BASIC ROM
WA1A6:
A1A6  84 59      STY  $59                   Scratch for numeric operation
A1A8  A9 00      LDA  #$00                  
A1AA  8D AD 02   STA  $02AD                 Not used
A1AD  20 82 A0   JSR  $A082                 BASIC ROM
A1B0  F0 FB      BEQ  $A1AD                 BASIC ROM
A1B2  C9 0D      CMP  #$0D                  
A1B4  D0 0D      BNE  $A1C3                 BASIC ROM
A1B6  A0 01      LDY  #$01                  
A1B8  A9 01      LDA  #$01                  
A1BA  91 FD      STA  ($FD),Y               
A1BC  A9 0D      LDA  #$0D                  
A1BE  20 CC A0   JSR  WA0CC                 BASIC ROM
A1C1  18         CLC                        
A1C2  60         RTS                        

WA1C3:
A1C3  C9 85      CMP  #$85                  
A1C5  90 0D      BCC  $A1D4                 BASIC ROM
A1C7  C9 8D      CMP  #$8D                  
A1C9  B0 09      BCS  WA1D4                 BASIC ROM
A1CB  A0 01      LDY  #$01                  
A1CD  91 FD      STA  ($FD),Y               
A1CF  38         SEC                        
A1D0  60         RTS                        

A1D1  4C AD A1   JMP  WA1AD                 BASIC ROM

WA1D4:
A1D4  C9 14      CMP  #$14                  
A1D6  D0 19      BNE  $A1F1                 BASIC ROM
A1D8  A4 59      LDY  $59                   Scratch for numeric operation
A1DA  F0 08      BEQ  $A1E4                 BASIC ROM
A1DC  20 CC A0   JSR  WA0CC                 BASIC ROM
A1DF  C6 59      DEC  $59                   Scratch for numeric operation
A1E1  4C AD A1   JMP  WA1AD                 BASIC ROM

WA1E4:
A1E4  AE B2 02   LDX  $02B2                 Not used
A1E7  F0 E8      BEQ  $A1D1                 BASIC ROM
A1E9  A0 00      LDY  #$00                  
A1EB  A9 FF      LDA  #$FF                  
A1ED  91 FD      STA  ($FD),Y               
A1EF  38         SEC                        
A1F0  60         RTS                        

WA1F1:
A1F1  C9 18      CMP  #$18                  
A1F3  D0 11      BNE  $A206                 BASIC ROM
A1F5  A4 59      LDY  $59                   Scratch for numeric operation
A1F7  F0 EB      BEQ  WA1E4                 BASIC ROM
A1F9  A9 14      LDA  #$14                  
A1FB  20 CC A0   JSR  WA0CC                 BASIC ROM
A1FE  88         DEY                        
A1FF  D0 FA      BNE  $A1FB                 BASIC ROM
A201  84 59      STY  $59                   Scratch for numeric operation
A203  4C AD A1   JMP  WA1AD                 BASIC ROM

WA206:
A206  C9 20      CMP  #$20                  
A208  90 C7      BCC  WA1D1                 BASIC ROM
A20A  C9 60      CMP  #$60                  
A20C  90 08      BCC  $A216                 BASIC ROM
A20E  C9 C0      CMP  #$C0                  
A210  90 BF      BCC  WA1D1                 BASIC ROM
A212  C9 DF      CMP  #$DF                  
A214  B0 BB      BCS  WA1D1                 BASIC ROM
WA216:
A216  A4 59      LDY  $59                   Scratch for numeric operation
A218  CC A8 02   CPY  $02A8                 Not used
A21B  90 48      BCC  $A265                 BASIC ROM
A21D  AE B2 02   LDX  $02B2                 Not used
A220  F0 AF      BEQ  WA1D1                 BASIC ROM
A222  C9 20      CMP  #$20                  
A224  D0 03      BNE  $A229                 BASIC ROM
A226  4C BC A1   JMP  $A1BC                 BASIC ROM

WA229:
A229  99 00 AF   STA  $AF00,Y               BASIC ROM
A22C  A6 59      LDX  $59                   Scratch for numeric operation
A22E  E8         INX                        
A22F  8E AD 02   STX  $02AD                 Not used
A232  A2 00      LDX  #$00                  
A234  B9 00 AF   LDA  $AF00,Y               BASIC ROM
A237  C9 20      CMP  #$20                  
A239  F0 09      BEQ  $A244                 BASIC ROM
A23B  88         DEY                        
A23C  C0 FF      CPY  #$FF                  
A23E  D0 F4      BNE  $A234                 BASIC ROM
A240  A4 59      LDY  $59                   Scratch for numeric operation
A242  D0 0E      BNE  $A252                 BASIC ROM
WA244:
A244  84 59      STY  $59                   Scratch for numeric operation
A246  C8         INY                        
A247  C0 01      CPY  #$01                  
A249  D0 02      BNE  $A24D                 BASIC ROM
A24B  84 59      STY  $59                   Scratch for numeric operation
WA24D:
A24D  A9 14      LDA  #$14                  
A24F  20 CC A0   JSR  WA0CC                 BASIC ROM
WA252:
A252  B9 00 AF   LDA  $AF00,Y               BASIC ROM
A255  9D 00 AE   STA  $AE00,X               BASIC ROM
A258  E8         INX                        
A259  C8         INY                        
A25A  CC AD 02   CPY  $02AD                 Not used
A25D  D0 EE      BNE  WA24D                 BASIC ROM
A25F  8E AD 02   STX  $02AD                 Not used
A262  4C BC A1   JMP  WA1BC                 BASIC ROM

WA265:
A265  99 00 AF   STA  $AF00,Y               BASIC ROM
A268  E6 59      INC  $59                   Scratch for numeric operation
A26A  AE A7 02   LDX  $02A7                 Not used
A26D  F0 02      BEQ  $A271                 BASIC ROM
A26F  A9 2E      LDA  #$2E                  
WA271:
A271  20 CC A0   JSR  WA0CC                 BASIC ROM
A274  4C AD A1   JMP  WA1AD                 BASIC ROM

WA277:
A277  A2 05      LDX  #$05                  
A279  20 C6 FF   JSR  $FFC6                 Routine: Open an input canal
A27C  20 CC FF   JSR  $FFCC                 Routine: Close the input and output channel
A27F  20 11 A1   JSR  $A111                 BASIC ROM
A282  A0 00      LDY  #$00                  
A284  CC AF 02   CPY  $02AF                 Not used
A287  F0 17      BEQ  $A2A0                 BASIC ROM
A289  B1 FB      LDA  ($FB),Y               Free 0 page for user program
A28B  C9 03      CMP  #$03                  
A28D  F0 0E      BEQ  $A29D                 BASIC ROM
A28F  20 CC A0   JSR  WA0CC                 BASIC ROM
A292  AD 9D 02   LDA  $029D                 RS-232 output buffer beginning (page)
A295  CD 9E 02   CMP  $029E                 RS-232 index for output buffer ending
A298  D0 F8      BNE  $A292                 BASIC ROM
A29A  C8         INY                        
A29B  D0 E7      BNE  $A284                 BASIC ROM
WA29D:
A29D  4C 49 CC   JMP  $CC49                 

WA2A0:
A2A0  A9 0D      LDA  #$0D                  
A2A2  A0 FF      LDY  #$FF                  
A2A4  D0 E9      BNE  $A28F                 BASIC ROM
WA2A6:
A2A6  20 82 A0   JSR  WA082                 BASIC ROM
A2A9  8D 00 AF   STA  $AF00                 BASIC ROM
A2AC  A9 01      LDA  #$01                  
A2AE  85 59      STA  $59                   Scratch for numeric operation
A2B0  A9 49      LDA  #$49                  
A2B2  20 88 A7   JSR  $A788                 BASIC ROM
A2B5  4C 49 CC   JMP  $CC49                 

WA2B8:
A2B8  A9 49      LDA  #$49                  
A2BA  20 13 A1   JSR  $A113                 BASIC ROM
A2BD  A0 00      LDY  #$00                  
A2BF  CC AF 02   CPY  $02AF                 Not used
A2C2  F0 11      BEQ  $A2D5                 BASIC ROM
A2C4  B1 FB      LDA  ($FB),Y               Free 0 page for user program
A2C6  C9 41      CMP  #$41                  
A2C8  90 08      BCC  $A2D2                 BASIC ROM
A2CA  C9 5B      CMP  #$5B                  
A2CC  B0 04      BCS  WA2D2                 BASIC ROM
A2CE  09 80      ORA  #$80                  
A2D0  91 FB      STA  ($FB),Y               Free 0 page for user program
WA2D2:
A2D2  C8         INY                        
A2D3  D0 EA      BNE  $A2BF                 BASIC ROM
WA2D5:
A2D5  4C 49 CC   JMP  $CC49                 

WA2D8:
A2D8  20 95 A0   JSR  $A095                 BASIC ROM
A2DB  F0 03      BEQ  $A2E0                 BASIC ROM
A2DD  20 2E A7   JSR  WA72E                 BASIC ROM
WA2E0:
A2E0  20 E4 FF   JSR  $FFE4                 Routine: Take the char from keyboard buffer
A2E3  F0 F3      BEQ  WA2D8                 BASIC ROM
A2E5  C9 93      CMP  #$93                  
A2E7  D0 03      BNE  $A2EC                 BASIC ROM
A2E9  4C 49 CC   JMP  $CC49                 

WA2EC:
A2EC  20 56 A0   JSR  WA056                 BASIC ROM
A2EF  20 4F AA   JSR  $AA4F                 BASIC ROM
A2F2  D0 E4      BNE  WA2D8                 BASIC ROM
A2F4  A9 28      LDA  #$28                  
A2F6  8D 01 D4   STA  $D401                 Voice 1: Frequency control (hi byte)
A2F9  A9 09      LDA  #$09                  
A2FB  8D 05 D4   STA  $D405                 Generator 1: Attack/Decay
A2FE  A9 00      LDA  #$00                  
A300  8D 06 D4   STA  $D406                 Generator 1: Sustain/Release
A303  8D 04 D4   STA  $D404                 Voice 1: Control registers
A306  A9 11      LDA  #$11                  
A308  8D 04 D4   STA  $D404                 Voice 1: Control registers
A30B  60         RTS                        

A30C  00         BRK                        
WA30D:
A30D  A2 08      LDX  #$08                  
A30F  20 C6 FF   JSR  $FFC6                 Routine: Open an input canal
A312  A0 00      LDY  #$00                  
A314  CC AC 02   CPY  $02AC                 Not used
A317  F0 09      BEQ  $A322                 BASIC ROM
A319  B9 00 AD   LDA  $AD00,Y               BASIC ROM
A31C  99 00 AF   STA  $AF00,Y               BASIC ROM
A31F  C8         INY                        
A320  D0 F2      BNE  $A314                 BASIC ROM
WA322:
A322  84 59      STY  $59                   Scratch for numeric operation
A324  A9 00      LDA  #$00                  
A326  8D AC 02   STA  $02AC                 Not used
A329  A9 30      LDA  #$30                  
A32B  20 4A A1   JSR  WA14A                 BASIC ROM
A32E  A9 00      LDA  #$00                  
A330  A8         TAY                        
A331  91 FD      STA  ($FD),Y               
A333  C8         INY                        
A334  91 FD      STA  ($FD),Y               
A336  20 B7 FF   JSR  $FFB7                 Routine: Read the I/O state word
A339  C9 00      CMP  #$00                  
A33B  F0 08      BEQ  $A345                 BASIC ROM
A33D  A0 01      LDY  #$01                  
A33F  A9 02      LDA  #$02                  
A341  91 FD      STA  ($FD),Y               
A343  D0 6E      BNE  $A3B3                 BASIC ROM
WA345:
A345  20 E4 FF   JSR  $FFE4                 Routine: Take the char from keyboard buffer
A348  C9 0A      CMP  #$0A                  
A34A  F0 EA      BEQ  $A336                 BASIC ROM
A34C  C9 A0      CMP  #$A0                  
A34E  D0 02      BNE  $A352                 BASIC ROM
A350  A9 20      LDA  #$20                  
WA352:
A352  C9 0D      CMP  #$0D                  
A354  D0 0F      BNE  $A365                 BASIC ROM
A356  20 B7 FF   JSR  $FFB7                 Routine: Read the I/O state word
A359  C9 00      CMP  #$00                  
A35B  D0 E0      BNE  $A33D                 BASIC ROM
A35D  A0 01      LDY  #$01                  
A35F  A9 01      LDA  #$01                  
A361  91 FD      STA  ($FD),Y               
A363  D0 4E      BNE  WA3B3                 BASIC ROM
WA365:
A365  A4 59      LDY  $59                   Scratch for numeric operation
A367  CC A8 02   CPY  $02A8                 Not used
A36A  F0 07      BEQ  $A373                 BASIC ROM
A36C  99 00 AF   STA  $AF00,Y               BASIC ROM
A36F  E6 59      INC  $59                   Scratch for numeric operation
A371  D0 C3      BNE  WA336                 BASIC ROM
WA373:
A373  C9 20      CMP  #$20                  
A375  F0 3C      BEQ  WA3B3                 BASIC ROM
A377  99 00 AF   STA  $AF00,Y               BASIC ROM
A37A  8D 00 AD   STA  $AD00                 BASIC ROM
A37D  B9 00 AF   LDA  $AF00,Y               BASIC ROM
A380  C9 20      CMP  #$20                  
A382  F0 0A      BEQ  $A38E                 BASIC ROM
A384  88         DEY                        
A385  10 F6      BPL  $A37D                 BASIC ROM
A387  A9 01      LDA  #$01                  
A389  8D AC 02   STA  $02AC                 Not used
A38C  D0 25      BNE  WA3B3                 BASIC ROM
WA38E:
A38E  84 57      STY  $57                   Scratch for numeric operation
A390  88         DEY                        
A391  30 F4      BMI  $A387                 BASIC ROM
A393  B9 00 AF   LDA  $AF00,Y               BASIC ROM
A396  C9 20      CMP  #$20                  
A398  F0 F6      BEQ  $A390                 BASIC ROM
A39A  A2 00      LDX  #$00                  
A39C  A4 57      LDY  $57                   Scratch for numeric operation
A39E  84 59      STY  $59                   Scratch for numeric operation
A3A0  C8         INY                        
A3A1  B9 00 AF   LDA  $AF00,Y               BASIC ROM
A3A4  9D 00 AD   STA  $AD00,X               BASIC ROM
A3A7  E8         INX                        
A3A8  C8         INY                        
A3A9  CC A8 02   CPY  $02A8                 Not used
A3AC  90 F3      BCC  $A3A1                 BASIC ROM
A3AE  F0 F1      BEQ  WA3A1                 BASIC ROM
A3B0  8E AC 02   STX  $02AC                 Not used
WA3B3:
A3B3  20 CC FF   JSR  $FFCC                 Routine: Close the input and output channel
A3B6  A0 01      LDY  #$01                  
A3B8  B1 FD      LDA  ($FD),Y               
A3BA  48         PHA                        
A3BB  A9 32      LDA  #$32                  
A3BD  20 4A A1   JSR  WA14A                 BASIC ROM
A3C0  A9 00      LDA  #$00                  
A3C2  A8         TAY                        
A3C3  91 FD      STA  ($FD),Y               
A3C5  C8         INY                        
A3C6  91 FD      STA  ($FD),Y               
A3C8  AD 00 AF   LDA  $AF00                 BASIC ROM
A3CB  C9 40      CMP  #$40                  
A3CD  D0 05      BNE  $A3D4                 BASIC ROM
A3CF  AD A9 02   LDA  $02A9                 Not used
A3D2  91 FD      STA  ($FD),Y               
WA3D4:
A3D4  68         PLA                        
A3D5  8D A9 02   STA  $02A9                 Not used
A3D8  20 86 A7   JSR  $A786                 BASIC ROM
A3DB  A2 05      LDX  #$05                  
A3DD  20 C6 FF   JSR  $FFC6                 Routine: Open an input canal
A3E0  20 CC FF   JSR  $FFCC                 Routine: Close the input and output channel
A3E3  4C 49 CC   JMP  $CC49                 

WA3E6:
A3E6  A2 08      LDX  #$08                  
A3E8  20 C6 FF   JSR  $FFC6                 Routine: Open an input canal
A3EB  A9 30      LDA  #$30                  
A3ED  20 4A A1   JSR  WA14A                 BASIC ROM
A3F0  A0 00      LDY  #$00                  
A3F2  98         TYA                        
A3F3  91 FD      STA  ($FD),Y               
A3F5  20 B7 FF   JSR  $FFB7                 Routine: Read the I/O state word
A3F8  C9 00      CMP  #$00                  
A3FA  F0 04      BEQ  $A400                 BASIC ROM
A3FC  A9 02      LDA  #$02                  
A3FE  D0 1A      BNE  $A41A                 BASIC ROM
WA400:
A400  20 CF FF   JSR  $FFCF                 Routine: Acept a char in the channel
A403  99 00 AF   STA  $AF00,Y               BASIC ROM
A406  C9 0D      CMP  #$0D                  
A408  D0 0B      BNE  $A415                 BASIC ROM
A40A  20 B7 FF   JSR  $FFB7                 Routine: Read the I/O state word
A40D  C9 00      CMP  #$00                  
A40F  D0 EB      BNE  $A3FC                 BASIC ROM
A411  A9 01      LDA  #$01                  
A413  D0 05      BNE  WA41A                 BASIC ROM
WA415:
A415  C8         INY                        
A416  C0 FF      CPY  #$FF                  
A418  D0 DB      BNE  $A3F5                 BASIC ROM
WA41A:
A41A  84 59      STY  $59                   Scratch for numeric operation
A41C  A0 01      LDY  #$01                  
A41E  91 FD      STA  ($FD),Y               
A420  20 CC FF   JSR  $FFCC                 Routine: Close the input and output channel
A423  4C D8 A3   JMP  $A3D8                 BASIC ROM

WA426:
A426  A9 01      LDA  #$01                  
A428  85 59      STA  $59                   Scratch for numeric operation
A42A  A2 04      LDX  #$04                  
A42C  20 C6 FF   JSR  $FFC6                 Routine: Open an input canal
A42F  20 CF FF   JSR  $FFCF                 Routine: Acept a char in the channel
A432  20 CF FF   JSR  $FFCF                 Routine: Acept a char in the channel
A435  F0 1A      BEQ  $A451                 BASIC ROM
A437  C9 04      CMP  #$04                  
A439  D0 1C      BNE  $A457                 BASIC ROM
A43B  20 CF FF   JSR  $FFCF                 Routine: Acept a char in the channel
A43E  20 CF FF   JSR  $FFCF                 Routine: Acept a char in the channel
A441  20 CF FF   JSR  $FFCF                 Routine: Acept a char in the channel
A444  20 CF FF   JSR  $FFCF                 Routine: Acept a char in the channel
A447  20 CF FF   JSR  $FFCF                 Routine: Acept a char in the channel
A44A  F0 E3      BEQ  $A42F                 BASIC ROM
A44C  20 B7 FF   JSR  $FFB7                 Routine: Read the I/O state word
A44F  F0 F6      BEQ  $A447                 BASIC ROM
WA451:
A451  A9 00      LDA  #$00                  
A453  85 59      STA  $59                   Scratch for numeric operation
A455  F0 5C      BEQ  $A4B3                 BASIC ROM
WA457:
A457  A9 32      LDA  #$32                  
A459  20 4A A1   JSR  WA14A                 BASIC ROM
A45C  20 CF FF   JSR  $FFCF                 Routine: Acept a char in the channel
A45F  A0 01      LDY  #$01                  
A461  91 FD      STA  ($FD),Y               
A463  20 CF FF   JSR  $FFCF                 Routine: Acept a char in the channel
A466  A0 00      LDY  #$00                  
A468  91 FD      STA  ($FD),Y               
A46A  20 CF FF   JSR  $FFCF                 Routine: Acept a char in the channel
A46D  C9 22      CMP  #$22                  
A46F  F0 0B      BEQ  $A47C                 BASIC ROM
A471  C9 42      CMP  #$42                  
A473  F0 2F      BEQ  $A4A4                 BASIC ROM
A475  20 B7 FF   JSR  $FFB7                 Routine: Read the I/O state word
A478  F0 F0      BEQ  $A46A                 BASIC ROM
A47A  D0 D5      BNE  WA451                 BASIC ROM
WA47C:
A47C  A2 01      LDX  #$01                  
A47E  20 CF FF   JSR  $FFCF                 Routine: Acept a char in the channel
A481  C9 22      CMP  #$22                  
A483  F0 0F      BEQ  $A494                 BASIC ROM
A485  E0 11      CPX  #$11                  
A487  B0 C8      BCS  WA451                 BASIC ROM
A489  9D 00 AF   STA  $AF00,X               BASIC ROM
A48C  20 B7 FF   JSR  $FFB7                 Routine: Read the I/O state word
A48F  D0 C0      BNE  WA451                 BASIC ROM
A491  E8         INX                        
A492  D0 EA      BNE  $A47E                 BASIC ROM
WA494:
A494  86 59      STX  $59                   Scratch for numeric operation
A496  20 CF FF   JSR  $FFCF                 Routine: Acept a char in the channel
A499  C9 20      CMP  #$20                  
A49B  D0 07      BNE  WA4A4                 BASIC ROM
A49D  20 B7 FF   JSR  $FFB7                 Routine: Read the I/O state word
A4A0  D0 AF      BNE  WA451                 BASIC ROM
A4A2  F0 F2      BEQ  $A496                 BASIC ROM
WA4A4:
A4A4  8D 00 AF   STA  $AF00                 BASIC ROM
A4A7  20 CF FF   JSR  $FFCF                 Routine: Acept a char in the channel
A4AA  F0 07      BEQ  WA4B3                 BASIC ROM
A4AC  20 B7 FF   JSR  $FFB7                 Routine: Read the I/O state word
A4AF  F0 F6      BEQ  $A4A7                 BASIC ROM
A4B1  D0 9E      BNE  WA451                 BASIC ROM
WA4B3:
A4B3  20 CC FF   JSR  $FFCC                 Routine: Close the input and output channel
A4B6  20 86 A7   JSR  WA786                 BASIC ROM
A4B9  4C 49 CC   JMP  $CC49                 

WA4BC:
A4BC  A9 FF      LDA  #$FF                  
A4BE  8D B5 02   STA  $02B5                 Not used
A4C1  A9 FA      LDA  #$FA                  
A4C3  8D B4 02   STA  $02B4                 Not used
A4C6  A9 EC      LDA  #$EC                  
A4C8  8D FA FF   STA  $FFFA                 Not Maskerable Interrupt (NMI) vector
A4CB  A9 A4      LDA  #$A4                  
A4CD  8D FB FF   STA  $FFFB                 
A4D0  A9 00      LDA  #$00                  
A4D2  8D 03 A5   STA  $A503                 BASIC ROM
A4D5  A9 D0      LDA  #$D0                  
A4D7  8D 04 A5   STA  $A504                 BASIC ROM
A4DA  78         SEI                        
A4DB  A9 34      LDA  #$34                  
A4DD  85 01      STA  $01                   6510 I/O register
A4DF  A9 FF      LDA  #$FF                  
A4E1  8D 00 D0   STA  $D000                 Position X sprite 0
A4E4  A9 36      LDA  #$36                  
A4E6  85 01      STA  $01                   6510 I/O register
A4E8  58         CLI                        
A4E9  4C 49 CC   JMP  $CC49                 

A4EC  48         PHA                        
A4ED  A5 01      LDA  $01                   6510 I/O register
A4EF  48         PHA                        
A4F0  A9 36      LDA  #$36                  
A4F2  85 01      STA  $01                   6510 I/O register
A4F4  A9 A4      LDA  #$A4                  
A4F6  48         PHA                        
A4F7  A9 FE      LDA  #$FE                  
A4F9  48         PHA                        
A4FA  08         PHP                        
A4FB  6C FA FF   JMP  ($FFFA)               Not Maskerable Interrupt (NMI) vector

A4FE  68         PLA                        
A4FF  85 01      STA  $01                   6510 I/O register
A501  68         PLA                        
A502  40         RTI                        

WA503:
A503  00         BRK                        
WA504:
A504  00         BRK                        
A505  00         BRK                        
A506  00         BRK                        
A507  AD 06 A5   LDA  $A506                 BASIC ROM
A50A  10 0D      BPL  $A519                 BASIC ROM
A50C  AD 03 A5   LDA  WA503                 BASIC ROM
A50F  85 FD      STA  $FD                   
A511  AD 04 A5   LDA  WA504                 BASIC ROM
A514  85 FE      STA  $FE                   Free 0 page for user program
A516  A9 FF      LDA  #$FF                  
A518  60         RTS                        

WA519:
A519  A9 00      LDA  #$00                  
A51B  85 FD      STA  $FD                   
A51D  A9 D0      LDA  #$D0                  
A51F  85 FE      STA  $FE                   Free 0 page for user program
A521  A9 00      LDA  #$00                  
A523  38         SEC                        
A524  ED 05 A5   SBC  $A505                 BASIC ROM
A527  85 57      STA  $57                   Scratch for numeric operation
A529  A9 00      LDA  #$00                  
A52B  ED 06 A5   SBC  WA506                 BASIC ROM
A52E  85 58      STA  $58                   Scratch for numeric operation
A530  A0 00      LDY  #$00                  
A532  B1 FD      LDA  ($FD),Y               
A534  AA         TAX                        
A535  C9 FF      CMP  #$FF                  
A537  F0 16      BEQ  $A54F                 BASIC ROM
A539  E6 57      INC  $57                   Scratch for numeric operation
A53B  D0 04      BNE  $A541                 BASIC ROM
A53D  E6 58      INC  $58                   Scratch for numeric operation
A53F  F0 0E      BEQ  WA54F                 BASIC ROM
WA541:
A541  8A         TXA                        
A542  38         SEC                        
A543  65 FD      ADC  $FD                   
A545  85 FD      STA  $FD                   
A547  A5 FE      LDA  $FE                   Free 0 page for user program
A549  69 00      ADC  #$00                  
A54B  85 FE      STA  $FE                   Free 0 page for user program
A54D  D0 E1      BNE  $A530                 BASIC ROM
WA54F:
A54F  8A         TXA                        
A550  60         RTS                        

A551  A5 59      LDA  $59                   Scratch for numeric operation
A553  C9 FF      CMP  #$FF                  
A555  F0 17      BEQ  $A56E                 BASIC ROM
A557  AD B4 02   LDA  $02B4                 Not used
A55A  18         CLC                        
A55B  ED 03 A5   SBC  WA503                 BASIC ROM
A55E  AA         TAX                        
A55F  AD B5 02   LDA  $02B5                 Not used
A562  A8         TAY                        
A563  ED 04 A5   SBC  WA504                 BASIC ROM
A566  D0 08      BNE  $A570                 BASIC ROM
A568  E4 59      CPX  $59                   Scratch for numeric operation
A56A  90 02      BCC  WA56E                 BASIC ROM
A56C  D0 02      BNE  WA570                 BASIC ROM
WA56E:
A56E  38         SEC                        
A56F  60         RTS                        

WA570:
A570  78         SEI                        
A571  A9 34      LDA  #$34                  
A573  85 01      STA  $01                   6510 I/O register
A575  20 07 A5   JSR  $A507                 BASIC ROM
A578  AD 03 A5   LDA  WA503                 BASIC ROM
A57B  85 FB      STA  $FB                   Free 0 page for user program
A57D  38         SEC                        
A57E  65 59      ADC  $59                   Scratch for numeric operation
A580  8D 03 A5   STA  WA503                 BASIC ROM
A583  AD 04 A5   LDA  WA504                 BASIC ROM
A586  85 FC      STA  $FC                   
A588  69 00      ADC  #$00                  
A58A  8D 04 A5   STA  WA504                 BASIC ROM
A58D  A4 59      LDY  $59                   Scratch for numeric operation
A58F  C8         INY                        
A590  A2 00      LDX  #$00                  
A592  A1 FB      LDA  ($FB,X)               Free 0 page for user program
A594  91 FB      STA  ($FB),Y               Free 0 page for user program
A596  A5 FB      LDA  $FB                   Free 0 page for user program
A598  C5 FD      CMP  $FD                   
A59A  D0 06      BNE  $A5A2                 BASIC ROM
A59C  A5 FC      LDA  $FC                   
A59E  C5 FE      CMP  $FE                   Free 0 page for user program
A5A0  F0 0B      BEQ  $A5AD                 BASIC ROM
WA5A2:
A5A2  A5 FB      LDA  $FB                   Free 0 page for user program
A5A4  D0 02      BNE  $A5A8                 BASIC ROM
A5A6  C6 FC      DEC  $FC                   
WA5A8:
A5A8  C6 FB      DEC  $FB                   Free 0 page for user program
A5AA  4C 92 A5   JMP  $A592                 BASIC ROM

WA5AD:
A5AD  A0 00      LDY  #$00                  
A5AF  A5 59      LDA  $59                   Scratch for numeric operation
A5B1  91 FD      STA  ($FD),Y               
A5B3  C4 59      CPY  $59                   Scratch for numeric operation
A5B5  F0 08      BEQ  $A5BF                 BASIC ROM
A5B7  B9 00 AF   LDA  $AF00,Y               BASIC ROM
A5BA  C8         INY                        
A5BB  91 FD      STA  ($FD),Y               
A5BD  D0 F4      BNE  $A5B3                 BASIC ROM
WA5BF:
A5BF  A9 36      LDA  #$36                  
A5C1  85 01      STA  $01                   6510 I/O register
A5C3  58         CLI                        
A5C4  18         CLC                        
A5C5  60         RTS                        

A5C6  78         SEI                        
A5C7  A9 34      LDA  #$34                  
A5C9  85 01      STA  $01                   6510 I/O register
A5CB  20 07 A5   JSR  WA507                 BASIC ROM
A5CE  C9 FF      CMP  #$FF                  
A5D0  F0 2E      BEQ  $A600                 BASIC ROM
A5D2  A8         TAY                        
A5D3  C8         INY                        
A5D4  84 57      STY  $57                   Scratch for numeric operation
A5D6  A2 00      LDX  #$00                  
A5D8  AD 03 A5   LDA  WA503                 BASIC ROM
A5DB  38         SEC                        
A5DC  E5 57      SBC  $57                   Scratch for numeric operation
A5DE  8D 03 A5   STA  WA503                 BASIC ROM
A5E1  B0 03      BCS  $A5E6                 BASIC ROM
A5E3  CE 04 A5   DEC  WA504                 BASIC ROM
WA5E6:
A5E6  B1 FD      LDA  ($FD),Y               
A5E8  81 FD      STA  ($FD,X)               
A5EA  A5 FD      LDA  $FD                   
A5EC  CD 03 A5   CMP  WA503                 BASIC ROM
A5EF  D0 07      BNE  $A5F8                 BASIC ROM
A5F1  A5 FE      LDA  $FE                   Free 0 page for user program
A5F3  CD 04 A5   CMP  WA504                 BASIC ROM
A5F6  F0 08      BEQ  WA600                 BASIC ROM
WA5F8:
A5F8  E6 FD      INC  $FD                   
A5FA  D0 EA      BNE  WA5E6                 BASIC ROM
A5FC  E6 FE      INC  $FE                   Free 0 page for user program
A5FE  D0 E6      BNE  WA5E6                 BASIC ROM
WA600:
A600  A9 36      LDA  #$36                  
A602  85 01      STA  $01                   6510 I/O register
A604  58         CLI                        
A605  60         RTS                        

A606  78         SEI                        
A607  A9 34      LDA  #$34                  
A609  85 01      STA  $01                   6510 I/O register
A60B  20 07 A5   JSR  WA507                 BASIC ROM
A60E  C9 FF      CMP  #$FF                  
A610  F0 16      BEQ  $A628                 BASIC ROM
A612  85 59      STA  $59                   Scratch for numeric operation
A614  A8         TAY                        
A615  B1 FD      LDA  ($FD),Y               
A617  88         DEY                        
A618  C0 FF      CPY  #$FF                  
A61A  F0 05      BEQ  $A621                 BASIC ROM
A61C  99 00 AF   STA  $AF00,Y               BASIC ROM
A61F  D0 F4      BNE  $A615                 BASIC ROM
WA621:
A621  A9 36      LDA  #$36                  
A623  85 01      STA  $01                   6510 I/O register
A625  58         CLI                        
A626  18         CLC                        
A627  60         RTS                        

WA628:
A628  A9 36      LDA  #$36                  
A62A  85 01      STA  $01                   6510 I/O register
A62C  58         CLI                        
A62D  38         SEC                        
A62E  60         RTS                        

WA62F:
A62F  A9 42      LDA  #$42                  
A631  20 13 A1   JSR  WA113                 BASIC ROM
A634  A5 FB      LDA  $FB                   Free 0 page for user program
A636  85 60      STA  $60                   Scratch for numeric operation
A638  A5 FC      LDA  $FC                   
A63A  85 61      STA  $61                   Floating point accumulator #1: Exponent
A63C  AD AF 02   LDA  $02AF                 Not used
A63F  F0 3E      BEQ  $A67F                 BASIC ROM
A641  8D 91 A6   STA  $A691                 BASIC ROM
A644  20 11 A1   JSR  WA111                 BASIC ROM
A647  AD AF 02   LDA  $02AF                 Not used
A64A  F0 33      BEQ  WA67F                 BASIC ROM
A64C  A9 00      LDA  #$00                  
A64E  85 57      STA  $57                   Scratch for numeric operation
A650  85 58      STA  $58                   Scratch for numeric operation
A652  A8         TAY                        
A653  B1 FB      LDA  ($FB),Y               Free 0 page for user program
A655  48         PHA                        
A656  98         TYA                        
A657  AA         TAX                        
A658  68         PLA                        
A659  A4 58      LDY  $58                   Scratch for numeric operation
A65B  D1 60      CMP  ($60),Y               Scratch for numeric operation
A65D  F0 08      BEQ  $A667                 BASIC ROM
A65F  A6 57      LDX  $57                   Scratch for numeric operation
A661  E6 57      INC  $57                   Scratch for numeric operation
A663  A0 00      LDY  #$00                  
A665  F0 06      BEQ  $A66D                 BASIC ROM
WA667:
A667  C8         INY                        
A668  CC 91 A6   CPY  WA691                 BASIC ROM
A66B  F0 0E      BEQ  $A67B                 BASIC ROM
WA66D:
A66D  84 58      STY  $58                   Scratch for numeric operation
A66F  8A         TXA                        
A670  A8         TAY                        
A671  C8         INY                        
A672  CC AF 02   CPY  $02AF                 Not used
A675  D0 DC      BNE  $A653                 BASIC ROM
A677  A9 00      LDA  #$00                  
A679  F0 04      BEQ  WA67F                 BASIC ROM
WA67B:
A67B  A6 57      LDX  $57                   Scratch for numeric operation
A67D  E8         INX                        
A67E  8A         TXA                        
WA67F:
A67F  48         PHA                        
A680  A9 31      LDA  #$31                  
A682  20 4A A1   JSR  WA14A                 BASIC ROM
A685  A9 00      LDA  #$00                  
A687  A8         TAY                        
A688  91 FD      STA  ($FD),Y               
A68A  C8         INY                        
A68B  68         PLA                        
A68C  91 FD      STA  ($FD),Y               
A68E  4C 49 CC   JMP  $CC49                 

WA691:
A691  00         BRK                        
A692  00         BRK                        
WA693:
A693  A9 42      LDA  #$42                  
A695  20 13 A1   JSR  WA113                 BASIC ROM
A698  AD AF 02   LDA  $02AF                 Not used
A69B  8D 91 A6   STA  WA691                 BASIC ROM
A69E  A0 00      LDY  #$00                  
A6A0  CC 91 A6   CPY  WA691                 BASIC ROM
A6A3  F0 08      BEQ  $A6AD                 BASIC ROM
A6A5  B1 FB      LDA  ($FB),Y               Free 0 page for user program
A6A7  99 00 AD   STA  $AD00,Y               BASIC ROM
A6AA  C8         INY                        
A6AB  D0 F3      BNE  $A6A0                 BASIC ROM
WA6AD:
A6AD  A9 43      LDA  #$43                  
A6AF  20 13 A1   JSR  WA113                 BASIC ROM
A6B2  A5 FB      LDA  $FB                   Free 0 page for user program
A6B4  85 60      STA  $60                   Scratch for numeric operation
A6B6  A5 FC      LDA  $FC                   
A6B8  85 61      STA  $61                   Floating point accumulator #1: Exponent
A6BA  AD AF 02   LDA  $02AF                 Not used
A6BD  8D 92 A6   STA  $A692                 BASIC ROM
A6C0  20 11 A1   JSR  WA111                 BASIC ROM
A6C3  A9 00      LDA  #$00                  
A6C5  85 57      STA  $57                   Scratch for numeric operation
A6C7  85 59      STA  $59                   Scratch for numeric operation
A6C9  A4 57      LDY  $57                   Scratch for numeric operation
A6CB  A2 00      LDX  #$00                  
A6CD  CC AF 02   CPY  $02AF                 Not used
A6D0  F0 56      BEQ  $A728                 BASIC ROM
A6D2  EC 91 A6   CPX  WA691                 BASIC ROM
A6D5  F0 1A      BEQ  $A6F1                 BASIC ROM
A6D7  B1 FB      LDA  ($FB),Y               Free 0 page for user program
A6D9  DD 00 AD   CMP  $AD00,X               BASIC ROM
A6DC  D0 04      BNE  $A6E2                 BASIC ROM
A6DE  C8         INY                        
A6DF  E8         INX                        
A6E0  D0 F0      BNE  $A6D2                 BASIC ROM
WA6E2:
A6E2  A4 57      LDY  $57                   Scratch for numeric operation
A6E4  B1 FB      LDA  ($FB),Y               Free 0 page for user program
A6E6  A6 59      LDX  $59                   Scratch for numeric operation
A6E8  9D 00 AF   STA  $AF00,X               BASIC ROM
A6EB  E6 59      INC  $59                   Scratch for numeric operation
A6ED  E6 57      INC  $57                   Scratch for numeric operation
A6EF  D0 D8      BNE  $A6C9                 BASIC ROM
WA6F1:
A6F1  E0 00      CPX  #$00                  
A6F3  F0 ED      BEQ  WA6E2                 BASIC ROM
A6F5  AD AF 02   LDA  $02AF                 Not used
A6F8  38         SEC                        
A6F9  ED 91 A6   SBC  WA691                 BASIC ROM
A6FC  90 E4      BCC  WA6E2                 BASIC ROM
A6FE  E5 57      SBC  $57                   Scratch for numeric operation
A700  90 E0      BCC  WA6E2                 BASIC ROM
A702  18         CLC                        
A703  65 59      ADC  $59                   Scratch for numeric operation
A705  6D 92 A6   ADC  WA692                 BASIC ROM
A708  B0 D8      BCS  WA6E2                 BASIC ROM
A70A  A0 00      LDY  #$00                  
A70C  A6 59      LDX  $59                   Scratch for numeric operation
A70E  CC 92 A6   CPY  WA692                 BASIC ROM
A711  F0 09      BEQ  $A71C                 BASIC ROM
A713  B1 60      LDA  ($60),Y               Scratch for numeric operation
A715  9D 00 AF   STA  $AF00,X               BASIC ROM
A718  C8         INY                        
A719  E8         INX                        
A71A  D0 F2      BNE  $A70E                 BASIC ROM
WA71C:
A71C  86 59      STX  $59                   Scratch for numeric operation
A71E  AD 91 A6   LDA  WA691                 BASIC ROM
A721  18         CLC                        
A722  65 57      ADC  $57                   Scratch for numeric operation
A724  85 57      STA  $57                   Scratch for numeric operation
A726  D0 A1      BNE  WA6C9                 BASIC ROM
WA728:
A728  20 86 A7   JSR  WA786                 BASIC ROM
A72B  4C 49 CC   JMP  $CC49                 

WA72E:
A72E  48         PHA                        
A72F  8A         TXA                        
A730  48         PHA                        
A731  98         TYA                        
A732  48         PHA                        
A733  A4 D3      LDY  $D3                   Column of cursor on the current line
A735  B1 D1      LDA  ($D1),Y               Pointer: current screen line address
A737  29 7F      AND  #$7F                  
A739  91 D1      STA  ($D1),Y               Pointer: current screen line address
A73B  BA         TSX                        
A73C  BD 03 01   LDA  $0103,X               CPU stack/Tape error/Floating conversion area
A73F  C9 14      CMP  #$14                  
A741  D0 02      BNE  $A745                 BASIC ROM
A743  A9 9D      LDA  #$9D                  
WA745:
A745  A0 FF      LDY  #$FF                  
A747  C9 09      CMP  #$09                  
A749  D0 07      BNE  $A752                 BASIC ROM
A74B  A5 D3      LDA  $D3                   Column of cursor on the current line
A74D  09 F8      ORA  #$F8                  
A74F  A8         TAY                        
A750  A9 20      LDA  #$20                  
WA752:
A752  C9 0D      CMP  #$0D                  
A754  F0 04      BEQ  $A75A                 BASIC ROM
A756  C9 20      CMP  #$20                  
A758  90 0E      BCC  $A768                 BASIC ROM
WA75A:
A75A  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
A75D  C8         INY                        
A75E  D0 FA      BNE  WA75A                 BASIC ROM
A760  A9 00      LDA  #$00                  
A762  85 D4      STA  $D4                   Flag: Editor mode "quotation", case 0x00=NO
A764  85 D8      STA  $D8                   Flag: Insert mode
A766  85 C7      STA  $C7                   Flag: Write inverse chars: 1=yes 0=not used
WA768:
A768  A4 D3      LDY  $D3                   Column of cursor on the current line
A76A  B1 D1      LDA  ($D1),Y               Pointer: current screen line address
A76C  09 80      ORA  #$80                  
A76E  91 D1      STA  ($D1),Y               Pointer: current screen line address
A770  A5 D2      LDA  $D2                   Pointer: current screen line address
A772  AA         TAX                        
A773  29 03      AND  #$03                  
A775  09 D8      ORA  #$D8                  
A777  85 D2      STA  $D2                   Pointer: current screen line address
A779  AD 86 02   LDA  $0286                 Current char color code
A77C  91 D1      STA  ($D1),Y               Pointer: current screen line address
A77E  86 D2      STX  $D2                   Pointer: current screen line address
A780  68         PLA                        
A781  A8         TAY                        
A782  68         PLA                        
A783  AA         TAX                        
A784  68         PLA                        
A785  60         RTS                        

WA786:
A786  A9 41      LDA  #$41                  
WA788:
A788  20 13 A1   JSR  WA113                 BASIC ROM
A78B  A9 02      LDA  #$02                  
A78D  8D DA A7   STA  $A7DA                 BASIC ROM
A790  A0 02      LDY  #$02                  
A792  A5 59      LDA  $59                   Scratch for numeric operation
A794  91 FD      STA  ($FD),Y               
A796  F0 41      BEQ  $A7D9                 BASIC ROM
A798  A5 33      LDA  $33                   Pointer: BASIC starting strings
A79A  18         CLC                        
A79B  E5 31      SBC  $31                   Pointer: BASIC ending arrays
A79D  AA         TAX                        
A79E  A5 34      LDA  $34                   Pointer: BASIC starting strings
A7A0  E5 32      SBC  $32                   Pointer: BASIC ending arrays
A7A2  D0 0F      BNE  $A7B3                 BASIC ROM
A7A4  E4 59      CPX  $59                   Scratch for numeric operation
A7A6  B0 0B      BCS  WA7B3                 BASIC ROM
A7A8  CE DA A7   DEC  WA7DA                 BASIC ROM
A7AB  F0 2C      BEQ  WA7D9                 BASIC ROM
A7AD  20 46 CC   JSR  $CC46                 
A7B0  4C 98 A7   JMP  $A798                 BASIC ROM

WA7B3:
A7B3  A5 33      LDA  $33                   Pointer: BASIC starting strings
A7B5  38         SEC                        
A7B6  E5 59      SBC  $59                   Scratch for numeric operation
A7B8  85 33      STA  $33                   Pointer: BASIC starting strings
A7BA  A0 03      LDY  #$03                  
A7BC  91 FD      STA  ($FD),Y               
A7BE  A5 34      LDA  $34                   Pointer: BASIC starting strings
A7C0  E9 00      SBC  #$00                  
A7C2  85 34      STA  $34                   Pointer: BASIC starting strings
A7C4  C8         INY                        
A7C5  91 FD      STA  ($FD),Y               
A7C7  A0 00      LDY  #$00                  
A7C9  B9 00 AF   LDA  $AF00,Y               BASIC ROM
A7CC  C9 22      CMP  #$22                  
A7CE  D0 02      BNE  $A7D2                 BASIC ROM
A7D0  A9 01      LDA  #$01                  
WA7D2:
A7D2  91 33      STA  ($33),Y               Pointer: BASIC starting strings
A7D4  C8         INY                        
A7D5  C4 59      CPY  $59                   Scratch for numeric operation
A7D7  D0 F0      BNE  $A7C9                 BASIC ROM
WA7D9:
A7D9  60         RTS                        

WA7DA:
A7DA  00         BRK                        
WA7DB:
A7DB  20 11 A1   JSR  WA111                 BASIC ROM
A7DE  AD AF 02   LDA  $02AF                 Not used
A7E1  85 59      STA  $59                   Scratch for numeric operation
A7E3  A0 00      LDY  #$00                  
A7E5  CC AF 02   CPY  $02AF                 Not used
A7E8  F0 08      BEQ  $A7F2                 BASIC ROM
A7EA  B1 FB      LDA  ($FB),Y               Free 0 page for user program
A7EC  99 00 AF   STA  $AF00,Y               BASIC ROM
A7EF  C8         INY                        
A7F0  D0 F3      BNE  $A7E5                 BASIC ROM
WA7F2:
A7F2  A9 30      LDA  #$30                  
A7F4  20 4A A1   JSR  WA14A                 BASIC ROM
A7F7  A0 01      LDY  #$01                  
A7F9  B1 FD      LDA  ($FD),Y               
A7FB  F0 09      BEQ  $A806                 BASIC ROM
A7FD  A4 59      LDY  $59                   Scratch for numeric operation
A7FF  A9 0D      LDA  #$0D                  
A801  99 00 AF   STA  $AF00,Y               BASIC ROM
A804  E6 59      INC  $59                   Scratch for numeric operation
WA806:
A806  20 28 A8   JSR  $A828                 BASIC ROM
A809  20 51 A5   JSR  $A551                 BASIC ROM
A80C  A9 32      LDA  #$32                  
A80E  20 4A A1   JSR  WA14A                 BASIC ROM
A811  AD B4 02   LDA  $02B4                 Not used
A814  18         CLC                        
A815  ED 03 A5   SBC  WA503                 BASIC ROM
A818  A0 01      LDY  #$01                  
A81A  91 FD      STA  ($FD),Y               
A81C  AD B5 02   LDA  $02B5                 Not used
A81F  ED 04 A5   SBC  WA504                 BASIC ROM
A822  88         DEY                        
A823  91 FD      STA  ($FD),Y               
A825  4C 49 CC   JMP  $CC49                 

WA828:
A828  A9 31      LDA  #$31                  
A82A  20 4A A1   JSR  WA14A                 BASIC ROM
A82D  A0 00      LDY  #$00                  
A82F  B1 FD      LDA  ($FD),Y               
A831  8D 06 A5   STA  WA506                 BASIC ROM
A834  C8         INY                        
A835  B1 FD      LDA  ($FD),Y               
A837  8D 05 A5   STA  WA505                 BASIC ROM
A83A  60         RTS                        

WA83B:
A83B  20 28 A8   JSR  WA828                 BASIC ROM
A83E  20 C6 A5   JSR  $A5C6                 BASIC ROM
A841  4C 0C A8   JMP  $A80C                 BASIC ROM

WA844:
A844  20 28 A8   JSR  WA828                 BASIC ROM
A847  20 06 A6   JSR  $A606                 BASIC ROM
A84A  90 04      BCC  $A850                 BASIC ROM
A84C  A9 00      LDA  #$00                  
A84E  85 59      STA  $59                   Scratch for numeric operation
WA850:
A850  A9 30      LDA  #$30                  
A852  20 4A A1   JSR  WA14A                 BASIC ROM
A855  A0 00      LDY  #$00                  
A857  98         TYA                        
A858  91 FD      STA  ($FD),Y               
A85A  A6 59      LDX  $59                   Scratch for numeric operation
A85C  F0 0E      BEQ  $A86C                 BASIC ROM
A85E  CA         DEX                        
A85F  BD 00 AF   LDA  $AF00,X               BASIC ROM
A862  A2 00      LDX  #$00                  
A864  C9 0D      CMP  #$0D                  
A866  D0 04      BNE  WA86C                 BASIC ROM
A868  A2 01      LDX  #$01                  
A86A  C6 59      DEC  $59                   Scratch for numeric operation
WA86C:
A86C  C8         INY                        
A86D  8A         TXA                        
A86E  91 FD      STA  ($FD),Y               
A870  20 86 A7   JSR  WA786                 BASIC ROM
A873  4C 49 CC   JMP  $CC49                 

WA876:
A876  A0 00      LDY  #$00                  
A878  AE B2 02   LDX  $02B2                 Not used
A87B  F0 16      BEQ  $A893                 BASIC ROM
A87D  20 11 A1   JSR  WA111                 BASIC ROM
A880  A0 00      LDY  #$00                  
A882  CC AF 02   CPY  $02AF                 Not used
A885  F0 0C      BEQ  WA893                 BASIC ROM
A887  B1 FB      LDA  ($FB),Y               Free 0 page for user program
A889  C9 03      CMP  #$03                  
A88B  F0 06      BEQ  WA893                 BASIC ROM
A88D  99 00 AF   STA  $AF00,Y               BASIC ROM
A890  C8         INY                        
A891  D0 EF      BNE  $A882                 BASIC ROM
WA893:
A893  84 59      STY  $59                   Scratch for numeric operation
A895  20 7E A1   JSR  $A17E                 BASIC ROM
A898  90 04      BCC  $A89E                 BASIC ROM
A89A  A9 00      LDA  #$00                  
A89C  85 59      STA  $59                   Scratch for numeric operation
WA89E:
A89E  20 86 A7   JSR  WA786                 BASIC ROM
A8A1  4C 49 CC   JMP  $CC49                 

WA8A4:
A8A4  20 82 A0   JSR  WA082                 BASIC ROM
A8A7  F0 FB      BEQ  WA8A4                 BASIC ROM
A8A9  C9 13      CMP  #$13                  
A8AB  F0 F7      BEQ  WA8A4                 BASIC ROM
A8AD  C9 85      CMP  #$85                  
A8AF  90 11      BCC  $A8C2                 BASIC ROM
A8B1  C9 8D      CMP  #$8D                  
A8B3  B0 0D      BCS  WA8C2                 BASIC ROM
A8B5  8D 00 AF   STA  $AF00                 BASIC ROM
A8B8  A9 01      LDA  #$01                  
A8BA  85 59      STA  $59                   Scratch for numeric operation
A8BC  20 86 A7   JSR  WA786                 BASIC ROM
A8BF  4C 49 CC   JMP  $CC49                 

WA8C2:
A8C2  20 CC A0   JSR  WA0CC                 BASIC ROM
A8C5  4C A4 A8   JMP  WA8A4                 BASIC ROM

A8C8  00         BRK                        
A8C9  00         BRK                        
A8CA  00         BRK                        
A8CB  00         BRK                        
A8CC  00         BRK                        
A8CD  00         BRK                        
A8CE  00         BRK                        
A8CF  00         BRK                        
A8D0  00         BRK                        
A8D1  00         BRK                        
A8D2  00         BRK                        
A8D3  00         BRK                        
A8D4  00         BRK                        
A8D5  A2 05      LDX  #$05                  
A8D7  20 C6 FF   JSR  $FFC6                 Routine: Open an input canal
A8DA  4C CC FF   JMP  $FFCC                 Routine: Close the input and output channel

A8DD  AD 9B 02   LDA  $029B                 RS-232 index for input buffer end
A8E0  CD 9C 02   CMP  $029C                 RS-232 input buffer beginning (page)
A8E3  D0 04      BNE  $A8E9                 BASIC ROM
A8E5  A9 00      LDA  #$00                  
A8E7  38         SEC                        
A8E8  60         RTS                        

WA8E9:
A8E9  8E 04 A9   STX  $A904                 BASIC ROM
A8EC  8C 05 A9   STY  $A905                 BASIC ROM
A8EF  A2 05      LDX  #$05                  
A8F1  20 C6 FF   JSR  $FFC6                 Routine: Open an input canal
A8F4  20 E4 FF   JSR  $FFE4                 Routine: Take the char from keyboard buffer
A8F7  48         PHA                        
A8F8  20 CC FF   JSR  $FFCC                 Routine: Close the input and output channel
A8FB  AE 04 A9   LDX  WA904                 BASIC ROM
A8FE  AC 05 A9   LDY  WA905                 BASIC ROM
A901  18         CLC                        
A902  68         PLA                        
A903  60         RTS                        

WA904:
A904  00         BRK                        
WA905:
A905  00         BRK                        
WA906:
A906  20 D5 A8   JSR  $A8D5                 BASIC ROM
A909  A9 00      LDA  #$00                  
A90B  8D C8 A8   STA  $A8C8                 BASIC ROM
A90E  A9 0A      LDA  #$0A                  
A910  8D D1 A8   STA  $A8D1                 BASIC ROM
A913  A9 15      LDA  #$15                  
A915  AE B0 02   LDX  $02B0                 Not used
A918  F0 02      BEQ  $A91C                 BASIC ROM
A91A  A9 43      LDA  #$43                  
WA91C:
A91C  8D D0 A8   STA  $A8D0                 BASIC ROM
A91F  A9 09      LDA  #$09                  
A921  8D CF A8   STA  $A8CF                 BASIC ROM
A924  AD D0 A8   LDA  WA8D0                 BASIC ROM
A927  20 4F AA   JSR  WAA4F                 BASIC ROM
A92A  20 6B AA   JSR  $AA6B                 BASIC ROM
A92D  20 DD A8   JSR  $A8DD                 BASIC ROM
A930  C9 01      CMP  #$01                  
A932  F0 41      BEQ  $A975                 BASIC ROM
A934  20 7D AA   JSR  $AA7D                 BASIC ROM
A937  C9 0A      CMP  #$0A                  
A939  D0 F2      BNE  $A92D                 BASIC ROM
A93B  CE CF A8   DEC  WA8CF                 BASIC ROM
A93E  D0 E4      BNE  $A924                 BASIC ROM
A940  F0 0F      BEQ  $A951                 BASIC ROM
A942  20 6B AA   JSR  WAA6B                 BASIC ROM
A945  20 DD A8   JSR  WA8DD                 BASIC ROM
A948  90 0A      BCC  $A954                 BASIC ROM
A94A  20 7D AA   JSR  WAA7D                 BASIC ROM
A94D  C9 78      CMP  #$78                  
A94F  D0 F4      BNE  $A945                 BASIC ROM
WA951:
A951  4C 3F AA   JMP  $AA3F                 BASIC ROM

WA954:
A954  C9 01      CMP  #$01                  
A956  F0 1D      BEQ  WA975                 BASIC ROM
A958  C9 18      CMP  #$18                  
A95A  F0 04      BEQ  $A960                 BASIC ROM
A95C  C9 04      CMP  #$04                  
A95E  D0 E5      BNE  WA945                 BASIC ROM
WA960:
A960  A9 06      LDA  #$06                  
A962  20 4F AA   JSR  WAA4F                 BASIC ROM
A965  A9 00      LDA  #$00                  
A967  8D 00 02   STA  $0200                 INPUT buffer of BASIC
A96A  A9 2A      LDA  #$2A                  
A96C  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
A96F  20 D5 A8   JSR  WA8D5                 BASIC ROM
A972  4C 49 CC   JMP  $CC49                 

WA975:
A975  20 6B AA   JSR  WAA6B                 BASIC ROM
A978  20 7D AA   JSR  WAA7D                 BASIC ROM
A97B  D0 24      BNE  $A9A1                 BASIC ROM
A97D  20 DD A8   JSR  WA8DD                 BASIC ROM
A980  B0 F6      BCS  $A978                 BASIC ROM
A982  8D C9 A8   STA  $A8C9                 BASIC ROM
A985  20 7D AA   JSR  WAA7D                 BASIC ROM
A988  D0 17      BNE  WA9A1                 BASIC ROM
A98A  20 DD A8   JSR  WA8DD                 BASIC ROM
A98D  B0 F6      BCS  $A985                 BASIC ROM
A98F  8D CA A8   STA  $A8CA                 BASIC ROM
A992  20 6B AA   JSR  WAA6B                 BASIC ROM
A995  A0 00      LDY  #$00                  
A997  20 DD A8   JSR  WA8DD                 BASIC ROM
A99A  90 08      BCC  $A9A4                 BASIC ROM
A99C  20 7D AA   JSR  WAA7D                 BASIC ROM
A99F  F0 F6      BEQ  $A997                 BASIC ROM
WA9A1:
A9A1  4C 35 AA   JMP  $AA35                 BASIC ROM

WA9A4:
A9A4  99 00 AF   STA  $AF00,Y               BASIC ROM
A9A7  20 6B AA   JSR  WAA6B                 BASIC ROM
A9AA  C8         INY                        
A9AB  C0 80      CPY  #$80                  
A9AD  D0 E8      BNE  WA997                 BASIC ROM
A9AF  A9 00      LDA  #$00                  
A9B1  8D CB A8   STA  $A8CB                 BASIC ROM
A9B4  8D CC A8   STA  $A8CC                 BASIC ROM
A9B7  20 6B AA   JSR  WAA6B                 BASIC ROM
A9BA  AC B0 02   LDY  $02B0                 Not used
A9BD  F0 02      BEQ  $A9C1                 BASIC ROM
A9BF  A0 01      LDY  #$01                  
WA9C1:
A9C1  20 DD A8   JSR  WA8DD                 BASIC ROM
A9C4  90 08      BCC  $A9CE                 BASIC ROM
A9C6  20 7D AA   JSR  WAA7D                 BASIC ROM
A9C9  F0 F6      BEQ  WA9C1                 BASIC ROM
A9CB  4C 35 AA   JMP  WAA35                 BASIC ROM

WA9CE:
A9CE  99 CB A8   STA  WA8CB,Y               BASIC ROM
A9D1  88         DEY                        
A9D2  10 ED      BPL  WA9C1                 BASIC ROM
A9D4  20 B4 AA   JSR  $AAB4                 BASIC ROM
A9D7  AD C9 A8   LDA  WA8C9                 BASIC ROM
A9DA  4D CA A8   EOR  WA8CA                 BASIC ROM
A9DD  C9 FF      CMP  #$FF                  
A9DF  D0 4E      BNE  $AA2F                 BASIC ROM
A9E1  AD CD A8   LDA  $A8CD                 BASIC ROM
A9E4  CD CB A8   CMP  WA8CB                 BASIC ROM
A9E7  D0 46      BNE  WAA2F                 BASIC ROM
A9E9  AD CE A8   LDA  $A8CE                 BASIC ROM
A9EC  CD CC A8   CMP  WA8CC                 BASIC ROM
A9EF  D0 3E      BNE  WAA2F                 BASIC ROM
A9F1  AC C9 A8   LDY  WA8C9                 BASIC ROM
A9F4  CC C8 A8   CPY  WA8C8                 BASIC ROM
A9F7  F0 1E      BEQ  $AA17                 BASIC ROM
A9F9  88         DEY                        
A9FA  CC C8 A8   CPY  WA8C8                 BASIC ROM
A9FD  D0 30      BNE  WAA2F                 BASIC ROM
A9FF  A2 02      LDX  #$02                  
AA01  20 C9 FF   JSR  $FFC9                 Routine: Open an output canal
AA04  A0 00      LDY  #$00                  
AA06  B9 00 AF   LDA  $AF00,Y               BASIC ROM
AA09  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
AA0C  C8         INY                        
AA0D  C0 80      CPY  #$80                  
AA0F  D0 F5      BNE  $AA06                 BASIC ROM
AA11  20 CC FF   JSR  $FFCC                 Routine: Close the input and output channel
AA14  20 D5 A8   JSR  WA8D5                 BASIC ROM
WAA17:
AA17  A9 06      LDA  #$06                  
AA19  20 4F AA   JSR  WAA4F                 BASIC ROM
AA1C  AD C9 A8   LDA  WA8C9                 BASIC ROM
AA1F  8D C8 A8   STA  WA8C8                 BASIC ROM
AA22  A9 0A      LDA  #$0A                  
AA24  8D D1 A8   STA  WA8D1                 BASIC ROM
AA27  A9 2D      LDA  #$2D                  
AA29  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
AA2C  4C 42 A9   JMP  $A942                 BASIC ROM

WAA2F:
AA2F  AD D0 A8   LDA  WA8D0                 BASIC ROM
AA32  20 4F AA   JSR  WAA4F                 BASIC ROM
WAA35:
AA35  A9 3A      LDA  #$3A                  
AA37  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
AA3A  CE D1 A8   DEC  WA8D1                 BASIC ROM
AA3D  D0 ED      BNE  $AA2C                 Routine: Value assignment to string variable (LET for strings)
WAA3F:
AA3F  A9 18      LDA  #$18                  
AA41  20 4F AA   JSR  WAA4F                 BASIC ROM
AA44  A9 01      LDA  #$01                  
AA46  8D 00 02   STA  $0200                 INPUT buffer of BASIC
AA49  20 D5 A8   JSR  WA8D5                 BASIC ROM
AA4C  4C 49 CC   JMP  $CC49                 

WAA4F:
AA4F  8E 04 A9   STX  WA904                 BASIC ROM
AA52  8C 05 A9   STY  WA905                 BASIC ROM
AA55  48         PHA                        
AA56  A2 05      LDX  #$05                  
AA58  20 C9 FF   JSR  $FFC9                 Routine: Open an output canal
AA5B  68         PLA                        
AA5C  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
AA5F  48         PHA                        
AA60  20 CC FF   JSR  $FFCC                 Routine: Close the input and output channel
AA63  AE 04 A9   LDX  WA904                 BASIC ROM
AA66  AC 05 A9   LDY  WA905                 BASIC ROM
AA69  68         PLA                        
AA6A  60         RTS                        

WAA6B:
AA6B  78         SEI                        
AA6C  A5 A0      LDA  $A0                   Real time clock HMS (1/60 sec)
AA6E  8D D2 A8   STA  $A8D2                 Routine: RETURN instruction
AA71  A5 A1      LDA  $A1                   Real time clock HMS (1/60 sec)
AA73  8D D3 A8   STA  $A8D3                 BASIC ROM
AA76  A5 A2      LDA  $A2                   Real time clock HMS (1/60 sec)
AA78  8D D4 A8   STA  $A8D4                 BASIC ROM
AA7B  58         CLI                        
AA7C  60         RTS                        

WAA7D:
AA7D  78         SEI                        
AA7E  38         SEC                        
AA7F  A5 A2      LDA  $A2                   Real time clock HMS (1/60 sec)
AA81  ED D4 A8   SBC  WA8D4                 BASIC ROM
AA84  8D 05 A9   STA  WA905                 BASIC ROM
AA87  A5 A1      LDA  $A1                   Real time clock HMS (1/60 sec)
AA89  ED D3 A8   SBC  WA8D3                 BASIC ROM
AA8C  8D 04 A9   STA  WA904                 BASIC ROM
AA8F  A5 A0      LDA  $A0                   Real time clock HMS (1/60 sec)
AA91  ED D2 A8   SBC  WA8D2                 Routine: RETURN instruction
AA94  B0 10      BCS  $AAA6                 BASIC ROM
AA96  AD 05 A9   LDA  WA905                 BASIC ROM
AA99  69 01      ADC  #$01                  
AA9B  8D 05 A9   STA  WA905                 BASIC ROM
AA9E  AD 04 A9   LDA  WA904                 BASIC ROM
AAA1  69 1A      ADC  #$1A                  
AAA3  8D 04 A9   STA  WA904                 BASIC ROM
WAAA6:
AAA6  AD 04 A9   LDA  WA904                 BASIC ROM
AAA9  0E 05 A9   ASL  WA905                 BASIC ROM
AAAC  2A         ROL                        
AAAD  0E 05 A9   ASL  WA905                 BASIC ROM
AAB0  2A         ROL                        
AAB1  58         CLI                        
AAB2  18         CLC                        
AAB3  60         RTS                        

WAAB4:
AAB4  AD D0 A8   LDA  WA8D0                 BASIC ROM
AAB7  C9 15      CMP  #$15                  
AAB9  D0 13      BNE  $AACE                 BASIC ROM
AABB  A9 00      LDA  #$00                  
AABD  A8         TAY                        
AABE  8D CE A8   STA  WA8CE                 BASIC ROM
AAC1  18         CLC                        
AAC2  79 00 AF   ADC  $AF00,Y               BASIC ROM
AAC5  C8         INY                        
AAC6  C0 80      CPY  #$80                  
AAC8  D0 F7      BNE  $AAC1                 BASIC ROM
AACA  8D CD A8   STA  WA8CD                 BASIC ROM
AACD  60         RTS                        

WAACE:
AACE  A0 00      LDY  #$00                  
AAD0  8C CD A8   STY  WA8CD                 BASIC ROM
AAD3  8C CE A8   STY  WA8CE                 BASIC ROM
AAD6  B9 00 AF   LDA  $AF00,Y               BASIC ROM
AAD9  8D 04 A9   STA  WA904                 BASIC ROM
AADC  20 E9 AA   JSR  $AAE9                 BASIC ROM
AADF  C8         INY                        
AAE0  10 F4      BPL  $AAD6                 BASIC ROM
AAE2  20 E9 AA   JSR  WAAE9                 BASIC ROM
AAE5  20 E9 AA   JSR  WAAE9                 BASIC ROM
AAE8  60         RTS                        

WAAE9:
AAE9  A2 08      LDX  #$08                  
AAEB  0E 04 A9   ASL  WA904                 BASIC ROM
AAEE  2E CD A8   ROL  WA8CD                 BASIC ROM
AAF1  2E CE A8   ROL  WA8CE                 BASIC ROM
AAF4  90 10      BCC  $AB06                 BASIC ROM
AAF6  AD CE A8   LDA  WA8CE                 BASIC ROM
AAF9  49 10      EOR  #$10                  
AAFB  8D CE A8   STA  WA8CE                 BASIC ROM
AAFE  AD CD A8   LDA  WA8CD                 BASIC ROM
AB01  49 21      EOR  #$21                  
AB03  8D CD A8   STA  WA8CD                 BASIC ROM
WAB06:
AB06  CA         DEX                        
AB07  D0 E2      BNE  $AAEB                 BASIC ROM
AB09  60         RTS                        

WAB0A:
AB0A  20 D5 A8   JSR  WA8D5                 BASIC ROM
AB0D  A9 01      LDA  #$01                  
AB0F  8D C9 A8   STA  WA8C9                 BASIC ROM
AB12  A9 00      LDA  #$00                  
AB14  8D C8 A8   STA  WA8C8                 BASIC ROM
AB17  A9 0A      LDA  #$0A                  
AB19  8D D1 A8   STA  WA8D1                 BASIC ROM
AB1C  20 6B AA   JSR  WAA6B                 BASIC ROM
AB1F  20 DD A8   JSR  WA8DD                 BASIC ROM
AB22  C9 18      CMP  #$18                  
AB24  F0 0F      BEQ  $AB35                 BASIC ROM
AB26  C9 15      CMP  #$15                  
AB28  F0 16      BEQ  $AB40                 BASIC ROM
AB2A  C9 43      CMP  #$43                  
AB2C  F0 12      BEQ  WAB40                 BASIC ROM
AB2E  20 7D AA   JSR  WAA7D                 BASIC ROM
AB31  C9 78      CMP  #$78                  
AB33  D0 EA      BNE  $AB1F                 BASIC ROM
WAB35:
AB35  A9 01      LDA  #$01                  
AB37  8D 00 02   STA  $0200                 INPUT buffer of BASIC
AB3A  20 D5 A8   JSR  WA8D5                 BASIC ROM
AB3D  4C 49 CC   JMP  $CC49                 

WAB40:
AB40  8D D0 A8   STA  WA8D0                 BASIC ROM
AB43  A2 02      LDX  #$02                  
AB45  20 C6 FF   JSR  $FFC6                 Routine: Open an input canal
AB48  A0 00      LDY  #$00                  
AB4A  AD C8 A8   LDA  WA8C8                 BASIC ROM
AB4D  F0 07      BEQ  $AB56                 BASIC ROM
AB4F  A9 1A      LDA  #$1A                  
AB51  99 00 AF   STA  $AF00,Y               BASIC ROM
AB54  D0 10      BNE  $AB66                 BASIC ROM
WAB56:
AB56  20 CF FF   JSR  $FFCF                 Routine: Acept a char in the channel
AB59  99 00 AF   STA  $AF00,Y               BASIC ROM
AB5C  20 B7 FF   JSR  $FFB7                 Routine: Read the I/O state word
AB5F  29 40      AND  #$40                  
AB61  F0 03      BEQ  WAB66                 BASIC ROM
AB63  8D C8 A8   STA  WA8C8                 BASIC ROM
WAB66:
AB66  C8         INY                        
AB67  10 E1      BPL  $AB4A                 BASIC ROM
AB69  20 CC FF   JSR  $FFCC                 Routine: Close the input and output channel
AB6C  20 D5 A8   JSR  WA8D5                 BASIC ROM
AB6F  A2 05      LDX  #$05                  
AB71  20 C9 FF   JSR  $FFC9                 Routine: Open an output canal
AB74  AD C8 A8   LDA  WA8C8                 BASIC ROM
AB77  10 07      BPL  $AB80                 BASIC ROM
AB79  A9 04      LDA  #$04                  
AB7B  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
AB7E  D0 31      BNE  $ABB1                 BASIC ROM
WAB80:
AB80  A9 01      LDA  #$01                  
AB82  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
AB85  AD C9 A8   LDA  WA8C9                 BASIC ROM
AB88  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
AB8B  49 FF      EOR  #$FF                  
AB8D  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
AB90  A0 00      LDY  #$00                  
AB92  B9 00 AF   LDA  $AF00,Y               BASIC ROM
AB95  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
AB98  C8         INY                        
AB99  10 F7      BPL  $AB92                 BASIC ROM
AB9B  20 B4 AA   JSR  WAAB4                 BASIC ROM
AB9E  AD D0 A8   LDA  WA8D0                 BASIC ROM
ABA1  C9 15      CMP  #$15                  
ABA3  F0 06      BEQ  $ABAB                 BASIC ROM
ABA5  AD CE A8   LDA  WA8CE                 BASIC ROM
ABA8  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
WABAB:
ABAB  AD CD A8   LDA  WA8CD                 BASIC ROM
ABAE  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
WABB1:
ABB1  20 CC FF   JSR  $FFCC                 Routine: Close the input and output channel
ABB4  AD 9D 02   LDA  $029D                 RS-232 output buffer beginning (page)
ABB7  CD 9E 02   CMP  $029E                 RS-232 index for output buffer ending
ABBA  D0 F8      BNE  $ABB4                 BASIC ROM
ABBC  20 6B AA   JSR  WAA6B                 BASIC ROM
ABBF  20 DD A8   JSR  WA8DD                 BASIC ROM
ABC2  C9 18      CMP  #$18                  
ABC4  D0 03      BNE  $ABC9                 BASIC ROM
ABC6  4C 35 AB   JMP  WAB35                 BASIC ROM

WABC9:
ABC9  C9 15      CMP  #$15                  
ABCB  F0 35      BEQ  $AC02                 BASIC ROM
ABCD  C9 43      CMP  #$43                  
ABCF  F0 31      BEQ  WAC02                 BASIC ROM
ABD1  C9 06      CMP  #$06                  
ABD3  F0 09      BEQ  $ABDE                 BASIC ROM
ABD5  20 7D AA   JSR  WAA7D                 BASIC ROM
ABD8  C9 1E      CMP  #$1E                  
ABDA  90 E3      BCC  $ABBF                 Routine: INPUT instruction
ABDC  B0 24      BCS  WAC02                 BASIC ROM
WABDE:
ABDE  EE C9 A8   INC  WA8C9                 BASIC ROM
ABE1  A9 0A      LDA  #$0A                  
ABE3  8D D1 A8   STA  WA8D1                 BASIC ROM
ABE6  A9 2D      LDA  #$2D                  
ABE8  AE C8 A8   LDX  WA8C8                 BASIC ROM
ABEB  10 02      BPL  $ABEF                 BASIC ROM
ABED  A9 2A      LDA  #$2A                  
WABEF:
ABEF  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
ABF2  8A         TXA                        
ABF3  D0 03      BNE  $ABF8                 BASIC ROM
ABF5  4C 43 AB   JMP  $AB43                 BASIC ROM

WABF8:
ABF8  30 15      BMI  $AC0F                 BASIC ROM
ABFA  A9 80      LDA  #$80                  
ABFC  8D C8 A8   STA  WA8C8                 BASIC ROM
ABFF  4C 6F AB   JMP  $AB6F                 BASIC ROM

WAC02:
AC02  A9 3A      LDA  #$3A                  
AC04  20 D2 FF   JSR  $FFD2                 Routine: Send a char in the channel
AC07  CE D1 A8   DEC  WA8D1                 BASIC ROM
AC0A  D0 F3      BNE  $ABFF                 BASIC ROM
AC0C  4C 35 AB   JMP  WAB35                 BASIC ROM

WAC0F:
AC0F  A9 00      LDA  #$00                  
AC11  8D 00 02   STA  $0200                 INPUT buffer of BASIC
AC14  4C 49 CC   JMP  $CC49                 

WAC17:
AC17  20 11 A1   JSR  WA111                 BASIC ROM
AC1A  20 6B AA   JSR  WAA6B                 BASIC ROM
AC1D  A0 00      LDY  #$00                  
AC1F  98         TYA                        
AC20  99 00 AF   STA  $AF00,Y               BASIC ROM
AC23  C8         INY                        
AC24  CC AF 02   CPY  $02AF                 Not used
AC27  D0 F7      BNE  $AC20                 BASIC ROM
AC29  20 95 A0   JSR  WA095                 BASIC ROM
AC2C  D0 0B      BNE  $AC39                 BASIC ROM
AC2E  20 7D AA   JSR  WAA7D                 BASIC ROM
AC31  C9 14      CMP  #$14                  
AC33  90 F4      BCC  $AC29                 BASIC ROM
AC35  A2 00      LDX  #$00                  
AC37  F0 2B      BEQ  $AC64                 BASIC ROM
WAC39:
AC39  20 2E A7   JSR  WA72E                 BASIC ROM
AC3C  AC AF 02   LDY  $02AF                 Not used
AC3F  99 00 AF   STA  $AF00,Y               BASIC ROM
AC42  A0 00      LDY  #$00                  
AC44  B9 01 AF   LDA  $AF01,Y               BASIC ROM
AC47  99 00 AF   STA  $AF00,Y               BASIC ROM
AC4A  C8         INY                        
AC4B  CC AF 02   CPY  $02AF                 Not used
AC4E  D0 F4      BNE  $AC44                 BASIC ROM
AC50  A0 00      LDY  #$00                  
AC52  B1 FB      LDA  ($FB),Y               Free 0 page for user program
AC54  20 49 A0   JSR  WA049                 BASIC ROM
AC57  D9 00 AF   CMP  $AF00,Y               BASIC ROM
AC5A  D0 CD      BNE  WAC29                 BASIC ROM
AC5C  C8         INY                        
AC5D  CC AF 02   CPY  $02AF                 Not used
AC60  D0 F0      BNE  $AC52                 BASIC ROM
AC62  A2 01      LDX  #$01                  
WAC64:
AC64  8E 00 AF   STX  $AF00                 BASIC ROM
AC67  A9 31      LDA  #$31                  
AC69  20 4A A1   JSR  WA14A                 BASIC ROM
AC6C  A0 00      LDY  #$00                  
AC6E  98         TYA                        
AC6F  91 FD      STA  ($FD),Y               
AC71  C8         INY                        
AC72  AD 00 AF   LDA  $AF00                 BASIC ROM
AC75  91 FD      STA  ($FD),Y               
AC77  4C 49 CC   JMP  $CC49                 

