Starting address=384


0384  78         SEI                        
0385  A9 D1      LDA  #$D1                  
0387  8D 14 03   STA  $0314                 Vector: Hardware Interrupt (IRQ)
038A  A9 03      LDA  #$03                  
038C  8D 15 03   STA  $0315                 Vector: Hardware Interrupt (IRQ)
038F  58         CLI                        
0390  60         RTS                        

0391  29 10      AND  #$10                 ;.A holds last value from $DD01. AND %00010000
                                           ;for the 1670 modem this is pin H from the user
                                           ;port.  If pin H is high there is no carrier,
                                           ;so we quit (the BNE on next line)
0393  D0 13      BNE  $03A8                ;on branch fail, reset system (see $03A8)
0395  EA         NOP                        
0396  EA         NOP                        
0397  A0 01      LDY  #$01                  
0399  B9 66 03   LDA  $0366,Y               Tape I/O buffer
039C  99 FF 03   STA  $03FF,Y               Not used
039F  C8         INY                        
03A0  CC 66 03   CPY  $0366                 Tape I/O buffer
03A3  D0 F4      BNE  $0399                 Tape I/O buffer
03A5  4C 31 EA   JMP  $EA31                 Default hardware interrupt (IRQ)

W03A8:
03A8  A0 00      LDY  #$00                 ;This loads the string "goto63"
03AA  B9 F4 03   LDA  $03F4,Y               Form $03F4 below into the keyboard buffer,
03AD  99 77 02   STA  $0277,Y               and then calls the regular BASIC routine
03B0  C8         INY                        to process what is in the buffer.
03B1  C0 0A      CPY  #$0A                  
03B3  D0 F5      BNE  $03AA                 
03B5  84 C6      STY  $C6                   Number of char in keyboard buffer
03B7  20 C4 03   JSR  $03C4                 Reset IRQ to default $EA31
03BA  A9 37      LDA  #$37                  Re-enable BASIC ROM
03BC  85 01      STA  $01                   
03BE  20 E7 FF   JSR  $FFE7                 Routine: Close all channels and files
03C1  4C 31 A8   JMP  $A831                 Jump to BASIC to handle

W03C4:
03C4  78         SEI                        
03C5  A9 31      LDA  #$31                  
03C7  8D 14 03   STA  $0314                 Vector: Hardware Interrupt (IRQ)
03CA  A9 EA      LDA  #$EA                  
03CC  8D 15 03   STA  $0315                 Vector: Hardware Interrupt (IRQ)
03CF  58         CLI                        
03D0  60         RTS                        

03D1  AD E0 03   LDA  $03E0                 Tape I/O buffer
03D4  C9 01      CMP  #$01                  
03D6  F0 BF      BEQ  $0397                 Tape I/O buffer
03D8  AD 01 DD   LDA  $DD01                 Data port B #2: user port, RS-232
03DB  4C 91 03   JMP  $0391                 Tape I/O buffer

03DE  00         BRK                        
03DF  00         BRK                        
W03E0:
03E0  00         BRK                        
03E1  00         BRK                        
03E2  00         BRK                        
03E3  00         BRK                        
03E4  00         BRK                        
03E5  00         BRK                        
03E6  00         BRK                        
03E7  00         BRK                        
03E8  00         BRK                        
03E9  00         BRK                        
03EA  00         BRK                        
03EB  00         BRK                        
03EC  00         BRK                        
03ED  00         BRK                        
03EE  00         BRK                        
03EF  00         BRK                        
03F0  00         BRK                        
03F1  00         BRK                        
03F2  00         BRK                        
03F3  00         BRK                        
W03F4:
03F4  08         PHP                        
03F5  0E 47 4F   ASL  $4F47                 
03F8  54 4F      NOOP $4F,X                 Scratch area
03FA  36 33      ROL  $33,X                 Pointer: BASIC starting strings
03FC  20 0D 0D   JSR  $0D0D                 
